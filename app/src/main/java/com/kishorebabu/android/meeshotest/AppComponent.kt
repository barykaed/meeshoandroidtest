package com.kishorebabu.mmnews


import com.kishorebabu.android.meeshotest.AppModule
import com.kishorebabu.android.meeshotest.features.pullrequests.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun inject(target: MainActivity)
}
