package com.kishorebabu.android.meeshotest.features.pullrequests

import com.kishorebabu.android.meeshotest.model.PullRequest


interface MainPresenter {
    fun setView(view: MainView)

    fun onViewReady()

    fun onViewDispose()

    fun onPullRequestClicked(pullRequestItem: PullRequest)

    fun onSearchClicked()
    fun onRepoNameUpdated(repoName: String)
    fun onRepoOwnerNameUpdated(repoOwnerName: String)
    fun onQueryOkClicked()

}
