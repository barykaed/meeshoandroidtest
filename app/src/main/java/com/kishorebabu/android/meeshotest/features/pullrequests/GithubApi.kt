package com.kishorebabu.android.meeshotest.features.pullrequests

import com.kishorebabu.android.meeshotest.model.PullRequest
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubApi {
    @GET("repos/{github_owner_name}/{github_repo_name}/pulls?state=open")
    fun getOpenPullRequestFeed(@Path("github_owner_name") githubUserName: String,
                               @Path("github_repo_name") repoName: String): Single<List<PullRequest>>
}