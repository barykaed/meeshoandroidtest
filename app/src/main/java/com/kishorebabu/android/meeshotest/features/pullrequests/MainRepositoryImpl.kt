package com.kishorebabu.android.meeshotest.features.pullrequests

import com.kishorebabu.android.meeshotest.model.PullRequest
import io.reactivex.Single

class MainRepositoryImpl(private var githubApi: GithubApi) : MainRepository {
    override fun getOpenPullRequestFeed(repoOwnerName: String, repoName: String): Single<List<PullRequest>> {
        return githubApi.getOpenPullRequestFeed(repoOwnerName, repoName)
    }


}
