package com.kishorebabu.android.meeshotest

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.kishorebabu.android.meeshotest.features.pullrequests.*

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Named("Base.Url")
    fun provideBaseUrl(): String {
        return "https://api.github.com/"
    }


    @Provides
    @Singleton
    fun provideRetrofit(@Named("Base.Url") baseUrl: String, okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    @Provides
    @Singleton
    fun provideGithubApi(retrofit: Retrofit): GithubApi =
            retrofit.create(GithubApi::class.java)

    @Provides
    @Singleton
    internal fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.connectTimeout(2L, TimeUnit.MINUTES)
        httpClientBuilder.readTimeout(2L, TimeUnit.MINUTES)
        httpClientBuilder.writeTimeout(2L, TimeUnit.MINUTES)

        if (BuildConfig.DEBUG) {
            httpClientBuilder.addInterceptor(httpLoggingInterceptor)
        }
        return httpClientBuilder.build()

    }

    @Provides
    @Singleton
    internal fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor { message ->
            Timber.d(message)
        }.setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create()

    }

    @Provides
    @Singleton
    fun provideMainRepository(newsApi: GithubApi): MainRepository {
        return MainRepositoryImpl(newsApi)
    }

    @Provides
    fun provideUserPresenter(userRepository: MainRepository): MainPresenter {
        return MainPresenterImpl(userRepository)
    }
}
