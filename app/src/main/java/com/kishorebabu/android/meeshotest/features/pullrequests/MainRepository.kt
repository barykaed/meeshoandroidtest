package com.kishorebabu.android.meeshotest.features.pullrequests

import com.kishorebabu.android.meeshotest.model.PullRequest
import io.reactivex.Single


interface MainRepository {
    fun getOpenPullRequestFeed(repoOwnerName: String, repoName: String): Single<List<PullRequest>>
}
