package com.kishorebabu.android.meeshotest.features.pullrequests

import android.app.ProgressDialog
import android.net.Uri
import android.os.Bundle
import android.support.customtabs.CustomTabsIntent
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.kishorebabu.android.meeshotest.MeeshoApplication
import com.kishorebabu.android.meeshotest.R
import com.kishorebabu.android.meeshotest.model.PullRequest
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter

    private lateinit var loadingView: ProgressDialog
    private lateinit var queryDialogView: View


    private lateinit var edtRepoName: EditText

    private lateinit var edtRepoOwnerName: EditText

    private lateinit var txtRepoPath: TextView

    private lateinit var alertDialog: AlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        setSupportActionBar(toolbar)
        loadingView = ProgressDialog(this)
        loadingView.isIndeterminate = true
        loadingView.setMessage(getString(R.string.loading))

        prepQueryDialogView()


        (application as MeeshoApplication).getComponent().inject(this)
        presenter.setView(this)

        presenter.onViewReady()
    }

    override fun showErrorNoOpenPRFound(repoOwnerName: String, repoName: String) {
        Snackbar.make(layout_root, "No Open PRs found for $repoOwnerName/$repoName", Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onViewDispose()
    }

    private fun prepQueryDialogView() {
        queryDialogView = this.layoutInflater.inflate(R.layout.dialog_github_repo_query, null)
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setView(queryDialogView)
        dialogBuilder.setTitle(R.string.query_dialog_title)
        dialogBuilder.setPositiveButton(R.string.ok, { _, _ ->
            presenter.onQueryOkClicked()
        })
        dialogBuilder.setNegativeButton(R.string.cancel, { dialog, _ ->
            dialog.dismiss()
        })
        alertDialog = dialogBuilder.create()
        edtRepoName = queryDialogView.findViewById(R.id.edtRepoName) as EditText
        edtRepoOwnerName = queryDialogView.findViewById(R.id.edtRepoOwnerName) as EditText
        txtRepoPath = queryDialogView.findViewById(R.id.txtPreviewPath) as TextView

        edtRepoName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable) {
                presenter.onRepoNameUpdated(text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

        })

        edtRepoOwnerName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable) {
                presenter.onRepoOwnerNameUpdated(text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
                presenter.onSearchClicked()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showPullRequestQueryView() {


        if (!isFinishing) {
            alertDialog.show()
        }


    }

    override fun showRepoPath(repoPathStr: String) {
        txtRepoPath.text = repoPathStr
    }

    override fun showPullRequestList(pullRequestList: List<PullRequest>) {
        rvPullRequestFeed.setHasFixedSize(true)
        rvPullRequestFeed.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rvPullRequestFeed.adapter = PullRequestFeedAdapter(pullRequestList, object : PullRequestFeedAdapter.PullRequestItemClickListener {
            override fun onPullRequestItemClicked(pullRequest: PullRequest) {
                presenter.onPullRequestClicked(pullRequest)
            }
        })
    }

    override fun openUrlView(url: String) {
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        builder.setToolbarColor(ContextCompat.getColor(this, R.color.colorAccent))
        customTabsIntent.launchUrl(this, Uri.parse(url))
    }

    override fun showErrorFailedToFetchPullRequests(repoOwnerName: String, repoName: String) {
        Snackbar.make(layout_root, "Failed to fetch PRs for $repoOwnerName/$repoName", Snackbar.LENGTH_SHORT).show()
    }

    override fun showLoadingView() {
        loadingView.show()
    }

    override fun hideLoadingView() {
        loadingView.dismiss()
    }
}
