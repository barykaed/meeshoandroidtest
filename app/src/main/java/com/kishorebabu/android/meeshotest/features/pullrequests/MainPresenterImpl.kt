package com.kishorebabu.android.meeshotest.features.pullrequests

import com.kishorebabu.android.meeshotest.model.PullRequest
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class MainPresenterImpl(private val mainRepository: MainRepository) : MainPresenter {
    private var view: MainView? = null
    private var repoOwnerName: String = ""
    private var repoName: String = ""
    private lateinit var compositeDisposable: CompositeDisposable

    private lateinit var pullRequestList: List<PullRequest>

    override fun setView(view: MainView) {
        this.view = view
    }

    override fun onViewReady() {

        compositeDisposable = CompositeDisposable()

        if (repoName.isNotEmpty()
                && repoOwnerName.isNotEmpty()) {
            view?.showLoadingView()

            mainRepository.getOpenPullRequestFeed(repoOwnerName, repoName)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            { pullRequestsList ->
                                this.pullRequestList = pullRequestsList
                                Timber.v("${pullRequestsList.size} pull requests fetched")

                                view?.let {
                                    if (pullRequestsList.isEmpty()) {
                                        view!!.showErrorNoOpenPRFound(repoOwnerName, repoName)
                                    } else {
                                        view!!.showPullRequestList(pullRequestsList)

                                    }

                                    view!!.hideLoadingView()

                                }
                            },
                            {
                                Timber.e(it, "Failed to fetch pull requests.")
                                view!!.hideLoadingView()
                                view?.showErrorFailedToFetchPullRequests(repoOwnerName, repoName)
                            }
                    ).addTo(compositeDisposable)
        } else {
            view?.showPullRequestQueryView()
        }
    }

    override fun onSearchClicked() {
        Timber.v("Search Clicked")
        view?.showPullRequestQueryView()

    }

    override fun onRepoNameUpdated(repoName: String) {
        this.repoName = repoName
        onRepoDetailsChanged(this.repoOwnerName, this.repoName)
    }

    private fun onRepoDetailsChanged(repoOwnerName: String, repoName: String) {
        val repoPathStr = "github.com/repos/$repoOwnerName/$repoName"
        view?.showRepoPath(repoPathStr)
    }

    override fun onQueryOkClicked() {
        Timber.v("Query OK Clicked on: $repoOwnerName $repoName")
        view?.showLoadingView()
        mainRepository.getOpenPullRequestFeed(repoOwnerName, repoName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { pullRequestsList ->
                            this.pullRequestList = pullRequestsList
                            Timber.v("${pullRequestsList.size} pull requests fetched")

                            view?.let {
                                view!!.showPullRequestList(pullRequestsList)
                                view!!.hideLoadingView()
                            }
                        },
                        {
                            Timber.e(it, "Failed to fetch pull requests.")
                            view!!.hideLoadingView()
                            view?.showErrorFailedToFetchPullRequests(repoOwnerName, repoName)
                        }
                ).addTo(compositeDisposable)
    }

    override fun onRepoOwnerNameUpdated(repoOwnerName: String) {
        this.repoOwnerName = repoOwnerName
        onRepoDetailsChanged(this.repoOwnerName, this.repoName)
    }

    override fun onPullRequestClicked(pullRequestItem: PullRequest) {
        Timber.v("Pull Request Item Clicked: ${pullRequestItem.title}")
        view?.openUrlView(pullRequestItem.htmlUrl!!)
    }


    override fun onViewDispose() {
        compositeDisposable.dispose()
    }

}
