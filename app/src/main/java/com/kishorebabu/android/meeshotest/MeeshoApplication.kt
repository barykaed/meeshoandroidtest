package com.kishorebabu.android.meeshotest

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.kishorebabu.mmnews.AppComponent
import com.kishorebabu.mmnews.DaggerAppComponent
import timber.log.Timber


class MeeshoApplication : Application() {

    private lateinit var component: AppComponent
    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder().appModule(AppModule()).build()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        AndroidThreeTen.init(this)
    }

    fun getComponent(): AppComponent {
        return component
    }
}