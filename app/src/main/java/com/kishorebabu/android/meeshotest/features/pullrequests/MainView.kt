package com.kishorebabu.android.meeshotest.features.pullrequests

import com.kishorebabu.android.meeshotest.model.PullRequest

interface MainView {
    fun showLoadingView()
    fun hideLoadingView()
    fun showPullRequestList(pullRequestList: List<PullRequest>)
    fun showErrorFailedToFetchPullRequests(repoOwnerName: String, repoName: String)
    fun openUrlView(url: String)
    fun showPullRequestQueryView()
    fun showRepoPath(repoPathStr: String)
    fun showErrorNoOpenPRFound(repoOwnerName: String, repoName: String)
}