package com.kishorebabu.android.meeshotest.model

import com.google.gson.annotations.SerializedName


data class PullRequest(
        @SerializedName("id") var id: Int?,
        @SerializedName("url") var url: String?,
        @SerializedName("html_url") var htmlUrl: String?,
        @SerializedName("diff_url") var diffUrl: String?,
        @SerializedName("patch_url") var patchUrl: String?,
        @SerializedName("issue_url") var issueUrl: String?,
        @SerializedName("commits_url") var commitsUrl: String?,
        @SerializedName("review_comments_url") var reviewCommentsUrl: String?,
        @SerializedName("review_comment_url") var reviewCommentUrl: String?,
        @SerializedName("comments_url") var commentsUrl: String?,
        @SerializedName("statuses_url") var statusesUrl: String?,
        @SerializedName("number") var number: Int?,
        @SerializedName("state") var state: String?,
        @SerializedName("title") var title: String?,
        @SerializedName("body") var body: String?,
        @SerializedName("assignee") var assignee: Assignee?,
        @SerializedName("labels") var labels: List<Label?>?,
        @SerializedName("milestone") var milestone: Milestone?,
        @SerializedName("locked") var locked: Boolean?,
        @SerializedName("active_lock_reason") var activeLockReason: String?,
        @SerializedName("created_at") var createdAt: String?,
        @SerializedName("updated_at") var updatedAt: String?,
        @SerializedName("closed_at") var closedAt: String?,
        @SerializedName("merged_at") var mergedAt: String?,
        @SerializedName("head") var head: Head?,
        @SerializedName("base") var base: Base?,
        @SerializedName("_links") var links: Links?,
        @SerializedName("user") var user: User?
)

data class Base(
        @SerializedName("label") var label: String?,
        @SerializedName("ref") var ref: String?,
        @SerializedName("sha") var sha: String?,
        @SerializedName("user") var user: User?,
        @SerializedName("repo") var repo: Repo?
)

data class Repo(
        @SerializedName("id") var id: Int?,
        @SerializedName("owner") var owner: Owner?,
        @SerializedName("name") var name: String?,
        @SerializedName("full_name") var fullName: String?,
        @SerializedName("description") var description: String?,
        @SerializedName("private") var private: Boolean?,
        @SerializedName("fork") var fork: Boolean?,
        @SerializedName("url") var url: String?,
        @SerializedName("html_url") var htmlUrl: String?,
        @SerializedName("archive_url") var archiveUrl: String?,
        @SerializedName("assignees_url") var assigneesUrl: String?,
        @SerializedName("blobs_url") var blobsUrl: String?,
        @SerializedName("branches_url") var branchesUrl: String?,
        @SerializedName("clone_url") var cloneUrl: String?,
        @SerializedName("collaborators_url") var collaboratorsUrl: String?,
        @SerializedName("comments_url") var commentsUrl: String?,
        @SerializedName("commits_url") var commitsUrl: String?,
        @SerializedName("compare_url") var compareUrl: String?,
        @SerializedName("contents_url") var contentsUrl: String?,
        @SerializedName("contributors_url") var contributorsUrl: String?,
        @SerializedName("deployments_url") var deploymentsUrl: String?,
        @SerializedName("downloads_url") var downloadsUrl: String?,
        @SerializedName("events_url") var eventsUrl: String?,
        @SerializedName("forks_url") var forksUrl: String?,
        @SerializedName("git_commits_url") var gitCommitsUrl: String?,
        @SerializedName("git_refs_url") var gitRefsUrl: String?,
        @SerializedName("git_tags_url") var gitTagsUrl: String?,
        @SerializedName("git_url") var gitUrl: String?,
        @SerializedName("hooks_url") var hooksUrl: String?,
        @SerializedName("issue_comment_url") var issueCommentUrl: String?,
        @SerializedName("issue_events_url") var issueEventsUrl: String?,
        @SerializedName("issues_url") var issuesUrl: String?,
        @SerializedName("keys_url") var keysUrl: String?,
        @SerializedName("labels_url") var labelsUrl: String?,
        @SerializedName("languages_url") var languagesUrl: String?,
        @SerializedName("merges_url") var mergesUrl: String?,
        @SerializedName("milestones_url") var milestonesUrl: String?,
        @SerializedName("mirror_url") var mirrorUrl: String?,
        @SerializedName("notifications_url") var notificationsUrl: String?,
        @SerializedName("pulls_url") var pullsUrl: String?,
        @SerializedName("releases_url") var releasesUrl: String?,
        @SerializedName("ssh_url") var sshUrl: String?,
        @SerializedName("stargazers_url") var stargazersUrl: String?,
        @SerializedName("statuses_url") var statusesUrl: String?,
        @SerializedName("subscribers_url") var subscribersUrl: String?,
        @SerializedName("subscription_url") var subscriptionUrl: String?,
        @SerializedName("svn_url") var svnUrl: String?,
        @SerializedName("tags_url") var tagsUrl: String?,
        @SerializedName("teams_url") var teamsUrl: String?,
        @SerializedName("trees_url") var treesUrl: String?,
        @SerializedName("homepage") var homepage: String?,
        @SerializedName("language") var language: Any?,
        @SerializedName("forks_count") var forksCount: Int?,
        @SerializedName("stargazers_count") var stargazersCount: Int?,
        @SerializedName("watchers_count") var watchersCount: Int?,
        @SerializedName("size") var size: Int?,
        @SerializedName("default_branch") var defaultBranch: String?,
        @SerializedName("open_issues_count") var openIssuesCount: Int?,
        @SerializedName("topics") var topics: List<String?>?,
        @SerializedName("has_issues") var hasIssues: Boolean?,
        @SerializedName("has_wiki") var hasWiki: Boolean?,
        @SerializedName("has_pages") var hasPages: Boolean?,
        @SerializedName("has_downloads") var hasDownloads: Boolean?,
        @SerializedName("archived") var archived: Boolean?,
        @SerializedName("pushed_at") var pushedAt: String?,
        @SerializedName("created_at") var createdAt: String?,
        @SerializedName("updated_at") var updatedAt: String?,
        @SerializedName("permissions") var permissions: Permissions?,
        @SerializedName("allow_rebase_merge") var allowRebaseMerge: Boolean?,
        @SerializedName("allow_squash_merge") var allowSquashMerge: Boolean?,
        @SerializedName("allow_merge_commit") var allowMergeCommit: Boolean?,
        @SerializedName("subscribers_count") var subscribersCount: Int?,
        @SerializedName("network_count") var networkCount: Int?
)

data class Owner(
        @SerializedName("login") var login: String?,
        @SerializedName("id") var id: Int?,
        @SerializedName("avatar_url") var avatarUrl: String?,
        @SerializedName("gravatar_id") var gravatarId: String?,
        @SerializedName("url") var url: String?,
        @SerializedName("html_url") var htmlUrl: String?,
        @SerializedName("followers_url") var followersUrl: String?,
        @SerializedName("following_url") var followingUrl: String?,
        @SerializedName("gists_url") var gistsUrl: String?,
        @SerializedName("starred_url") var starredUrl: String?,
        @SerializedName("subscriptions_url") var subscriptionsUrl: String?,
        @SerializedName("organizations_url") var organizationsUrl: String?,
        @SerializedName("repos_url") var reposUrl: String?,
        @SerializedName("events_url") var eventsUrl: String?,
        @SerializedName("received_events_url") var receivedEventsUrl: String?,
        @SerializedName("type") var type: String?,
        @SerializedName("site_admin") var siteAdmin: Boolean?
)

data class Permissions(
        @SerializedName("admin") var admin: Boolean?,
        @SerializedName("push") var push: Boolean?,
        @SerializedName("pull") var pull: Boolean?
)

data class User(
        @SerializedName("login") var login: String?,
        @SerializedName("id") var id: Int?,
        @SerializedName("avatar_url") var avatarUrl: String?,
        @SerializedName("gravatar_id") var gravatarId: String?,
        @SerializedName("url") var url: String?,
        @SerializedName("html_url") var htmlUrl: String?,
        @SerializedName("followers_url") var followersUrl: String?,
        @SerializedName("following_url") var followingUrl: String?,
        @SerializedName("gists_url") var gistsUrl: String?,
        @SerializedName("starred_url") var starredUrl: String?,
        @SerializedName("subscriptions_url") var subscriptionsUrl: String?,
        @SerializedName("organizations_url") var organizationsUrl: String?,
        @SerializedName("repos_url") var reposUrl: String?,
        @SerializedName("events_url") var eventsUrl: String?,
        @SerializedName("received_events_url") var receivedEventsUrl: String?,
        @SerializedName("type") var type: String?,
        @SerializedName("site_admin") var siteAdmin: Boolean?
)

data class Assignee(
        @SerializedName("login") var login: String?,
        @SerializedName("id") var id: Int?,
        @SerializedName("avatar_url") var avatarUrl: String?,
        @SerializedName("gravatar_id") var gravatarId: String?,
        @SerializedName("url") var url: String?,
        @SerializedName("html_url") var htmlUrl: String?,
        @SerializedName("followers_url") var followersUrl: String?,
        @SerializedName("following_url") var followingUrl: String?,
        @SerializedName("gists_url") var gistsUrl: String?,
        @SerializedName("starred_url") var starredUrl: String?,
        @SerializedName("subscriptions_url") var subscriptionsUrl: String?,
        @SerializedName("organizations_url") var organizationsUrl: String?,
        @SerializedName("repos_url") var reposUrl: String?,
        @SerializedName("events_url") var eventsUrl: String?,
        @SerializedName("received_events_url") var receivedEventsUrl: String?,
        @SerializedName("type") var type: String?,
        @SerializedName("site_admin") var siteAdmin: Boolean?
)

data class Label(
        @SerializedName("id") var id: Int?,
        @SerializedName("url") var url: String?,
        @SerializedName("name") var name: String?,
        @SerializedName("description") var description: String?,
        @SerializedName("color") var color: String?,
        @SerializedName("default") var default: Boolean?
)

data class Milestone(
        @SerializedName("url") var url: String?,
        @SerializedName("html_url") var htmlUrl: String?,
        @SerializedName("labels_url") var labelsUrl: String?,
        @SerializedName("id") var id: Int?,
        @SerializedName("number") var number: Int?,
        @SerializedName("state") var state: String?,
        @SerializedName("title") var title: String?,
        @SerializedName("description") var description: String?,
        @SerializedName("creator") var creator: Creator?,
        @SerializedName("open_issues") var openIssues: Int?,
        @SerializedName("closed_issues") var closedIssues: Int?,
        @SerializedName("created_at") var createdAt: String?,
        @SerializedName("updated_at") var updatedAt: String?,
        @SerializedName("closed_at") var closedAt: String?,
        @SerializedName("due_on") var dueOn: String?
)

data class Creator(
        @SerializedName("login") var login: String?,
        @SerializedName("id") var id: Int?,
        @SerializedName("avatar_url") var avatarUrl: String?,
        @SerializedName("gravatar_id") var gravatarId: String?,
        @SerializedName("url") var url: String?,
        @SerializedName("html_url") var htmlUrl: String?,
        @SerializedName("followers_url") var followersUrl: String?,
        @SerializedName("following_url") var followingUrl: String?,
        @SerializedName("gists_url") var gistsUrl: String?,
        @SerializedName("starred_url") var starredUrl: String?,
        @SerializedName("subscriptions_url") var subscriptionsUrl: String?,
        @SerializedName("organizations_url") var organizationsUrl: String?,
        @SerializedName("repos_url") var reposUrl: String?,
        @SerializedName("events_url") var eventsUrl: String?,
        @SerializedName("received_events_url") var receivedEventsUrl: String?,
        @SerializedName("type") var type: String?,
        @SerializedName("site_admin") var siteAdmin: Boolean?
)

data class Head(
        @SerializedName("label") var label: String?,
        @SerializedName("ref") var ref: String?,
        @SerializedName("sha") var sha: String?,
        @SerializedName("user") var user: User?,
        @SerializedName("repo") var repo: Repo?
)

data class Links(
        @SerializedName("self") var self: Self?,
        @SerializedName("html") var html: Html?,
        @SerializedName("issue") var issue: Issue?,
        @SerializedName("comments") var comments: Comments?,
        @SerializedName("review_comments") var reviewComments: ReviewComments?,
        @SerializedName("review_comment") var reviewComment: ReviewComment?,
        @SerializedName("commits") var commits: Commits?,
        @SerializedName("statuses") var statuses: Statuses?
)

data class Self(
        @SerializedName("href") var href: String?
)

data class Html(
        @SerializedName("href") var href: String?
)

data class Comments(
        @SerializedName("href") var href: String?
)

data class ReviewComments(
        @SerializedName("href") var href: String?
)

data class Issue(
        @SerializedName("href") var href: String?
)

data class Commits(
        @SerializedName("href") var href: String?
)

data class Statuses(
        @SerializedName("href") var href: String?
)

data class ReviewComment(
        @SerializedName("href") var href: String?
)