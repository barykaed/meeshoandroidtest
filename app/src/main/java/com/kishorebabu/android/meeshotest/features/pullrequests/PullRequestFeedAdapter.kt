package com.kishorebabu.android.meeshotest.features.pullrequests

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.kishorebabu.android.meeshotest.R
import com.kishorebabu.android.meeshotest.model.PullRequest
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

class PullRequestFeedAdapter(private var pullRequestList: List<PullRequest>,
                             private var pullrequestClickListener: PullRequestItemClickListener)
    : RecyclerView.Adapter<PullRequestFeedAdapter.PullRequestViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PullRequestViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pull_request_feed, parent, false)
        return PullRequestViewHolder(view)
    }

    override fun getItemCount(): Int {
        return pullRequestList.size
    }

    override fun onBindViewHolder(holder: PullRequestViewHolder, position: Int) {
        val timestamp = pullRequestList[position].createdAt
        val formattedTimestamp = LocalDateTime.parse(timestamp, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX")).format(DateTimeFormatter.ofPattern("dd MMM yyyy, hh:mm a"))
        holder.txtTitle.text = pullRequestList[position].title
        holder.txtByline.text = holder.itemView.resources.getString(R.string.adapter_byline_1, pullRequestList[position].number,
                pullRequestList[position].user?.login, formattedTimestamp)

        holder.txtByline2.text = holder.itemView.resources.getString(R.string.adapter_byline_2, pullRequestList[position].state?.toUpperCase())
        holder.itemView.setOnClickListener {
            pullrequestClickListener.onPullRequestItemClicked(pullRequestList[position])
        }
    }

    class PullRequestViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var txtTitle: TextView = itemView.findViewById<View>(R.id.txtTitle) as TextView
        var txtByline: TextView = itemView.findViewById<View>(R.id.txtByline) as TextView
        var txtByline2: TextView = itemView.findViewById<View>(R.id.txtByline_2) as TextView
    }

    interface PullRequestItemClickListener {
        fun onPullRequestItemClicked(pullRequest: PullRequest)
    }
}