# Meesho Android Test

## This project uses:
- [RxJava2](https://github.com/ReactiveX/RxJava) and [RxAndroid](https://github.com/ReactiveX/RxAndroid)
- [Retrofit](http://square.github.io/retrofit/) / [OkHttp](http://square.github.io/okhttp/)
- [Gson](https://github.com/square/moshi)
- [Dagger 2](http://google.github.io/dagger/)
- [Timber](https://github.com/JakeWharton/timber)
- [ThreeTenABP](https://github.com/JakeWharton/ThreeTenABP)

##Features

- On Launch, enter the Repo Owner name and Repo Name in the dialog.
- Click on any open PR, it will launch a custom Chrome Tab to launch the GitHub HTML url.


## Building

To build, install and run a debug version, run this from the root of the project:
```sh
./gradlew clean assembleDebug
```
